---
title: "Introduction"
date: 2021-05-18T11:22:35+02:00
draft: false
---


{{< hint danger >}}
**WARNING**

[User projects](/cloud/register/#personal-project) (generated for every user) are not meant to contain production machines.
If you use your personal project for long-term services, you have to to ask for a [GROUP](/cloud/register/#group-project)  project (even if you do not work in a group or you do not need any extra quotas).
{{</hint>}}



This guide aims to provide a walk-through for setting up a rudimentary
virtual infrastructure in MetaCentrum Cloud. It is a good jumping-off
point for most users.

MetaCentrum Cloud is the [IaaS cloud](https://en.wikipedia.org/wiki/Infrastructure_as_a_service) on top of [open-source OpenStack project](https://opendev.org/openstack).
Users may configure and use cloud resources for reaching individual (scientific) goals.

Most important cloud resources are:
 * virtual machines
 * virtual networking (VPNs, firewalls, routers)
 * private and/or public IP addresses
 * storage
 * cloud load balancers


The left sidebar can be used for navigation throughout the documentation.
The whole guide can also be downloaded as PDFs for printing or later use.

__New users__ should head over to the [Get Access](cloud/register)
section and make sure they have an active user account and required
permissions to access the service.

__Beginners__ should start in the [Quick Start](cloud/quick-start)
section which provides a step-by-step guide for starting the first
virtual machine instance.

__Advanced users__ should continue in the [Advanced Features](cloud/gui)
or [Command Line Interface](cloud/cli) sections, as these are
more suitable for complex use cases and exploration of available
features.

__Expert users__ with complex infrastructural or scientific use cases
should contact user support and request assistance specifically for
their use case.

__Frequently asked questions__ and corresponding answers can be found in
the [FAQ](cloud/faq) section. Please, consult this section before
contacting user support.

Bear in mind that this is not the complete documentation to OpenStack
but rather a quick guide that is supposed to help you with elementary
use of our infrastructure. If you need more information, please turn
to [the official documentation](https://docs.openstack.org/train/user/)
or contact user support and describe your use case.

Please visit [Network](cloud/network) section in order to see how you should set up the network.
