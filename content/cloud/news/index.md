---
title: "News"
date: 2021-05-18T11:22:35+02:00
draft: false
---


**2021-05-21** Flavor list was created and published. Also parameters of following flavors were changed:

* hpc.8core-64ram
* hpc.8core-16ram
* hpc.16core-32ram
* hpc.18core-48ram
* hpc.small
* hpc.medium
* hpc.large
* hpc.xlarge
* hpc.xlarge-memory
* hpc.16core-128ram
* hpc.30core-64ram
* hpc.30core-256ram
* hpc.ics-gladosag-full
* csirtmu.tiny1x2

None of the parameters were decreased but increased. Updated parameters were Net througput, IOPS and Disk througput. Existing instances will have the previous parameters so if you want to get new parameters, **make a data backup** and rebuild your instance  You can check list of flavors [here](/cloud/flavors).

**2021-04-13** OpenStack image `centos-8-1-1911-x86_64_gpu` deprecation in favor of `centos-8-x86_64_gpu`. Deprecated image will be still available for existing VM instances, but will be moved from public to community images in about 2 months.

**2021-04-05** OpenStack images renamed

**2021-03-31** User documentation update

**2020-07-24** Octavia service (LBaaS) released

**2020-06-11** [Public repository](https://gitlab.ics.muni.cz/cloud/cloud-tools) where Openstack users can find usefull tools

**2020-05-27** Openstack was updated from `stein` to `train` version

**2020-05-13** Ubuntu 20.04 LTS (Focal Fossa) available in image catalog

**2020-05-01** Released [Web page](https://projects.cloud.muni.cz/) for requesting Openstack projects
