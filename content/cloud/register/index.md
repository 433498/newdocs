---
title: "How to Get Access"
date: 2021-05-18T11:22:35+02:00
draft: false
---


Access to MetaCentrum Cloud is granted to users with active accounts in
one of the following identity federations
* __EINFRA CESNET__,
* __EGI Check-in__`*`,
* __ELIXIR AAI__`*`.
* __DEEP AAI__`*`.

Users from the Czech academic community should always use the `EINFRA CESNET`
identity provider, unless instructed otherwise by user support.
Identity providers marked with `*` should only be used by international
communities with explicitly negotiated resource allocations.

MetaCentrum Cloud provides the following ways for allocating resources
* __personal project__,
* __group project__.

## Personal Project

A personal project goal is to gain cloud environment knowledge, resource allocation quotas are severe and could not be extended.

{{< hint danger >}}
**WARNING**

A personal project resource allocation quotas are severe and could not be extended.
{{</hint>}}


Personal projects are available automatically to all users of the Czech
e-infrastructure for science, development, and education.

To register, follow instructions for
[registration in the MetaCentrum VO](https://metavo.metacentrum.cz/en/application/index.html).

Personal projects are intended as a low-barrier entry
into the infrastructure for testing and exploration of features. Any
serious resource usage requires the use of a group project, see below.

The following already established terms and conditions apply
* [Terms and Conditions for Access to the CESNET e-infrastructure](https://www.cesnet.cz/conditions/?lang=en)
* [MetaCentrum End User Statement and Usage Rules](https://www.metacentrum.cz/en/about/rules/index.html)
* [Appreciation Formula / Acknowlegement in Publications](https://wiki.metacentrum.cz/wiki/Usage_rules/Acknowledgement)
* [Account Validity](https://wiki.metacentrum.cz/wiki/Usage_rules/Account)
* [Annual Report](https://wiki.metacentrum.cz/wiki/MetaCentrum_Annual_Report_%E2%88%92_Author_Instructions)

## Group Project

{{< hint info >}}
**NOTICE**

Preferable way of requesting new <a href="https://cloud.gitlab-pages.ics.muni.cz/documentation/register/#group-project">GROUP</a> project is through this online application form: <a href="https://projects.cloud.muni.cz/">https://projects.cloud.muni.cz/</a>
{{</hint>}}


Group projects are the primary resource allocation unit for MetaCentrum Cloud.
Any user or a group of users requiring a non-trivial amount of resources must
request a group project from User Support and provide the following basic information:
* __name of the project__,
* __purpose of the project__,
* __contact information__,
* __amount and type of requested resources__ _[(please read first)](/cloud/faq/#how-many-floating-ips-does-my-group-project-need)_,
* __impact would the service have on unavailability for 1h, 1d, 1w__,
* __estimated length of the project__,
* __access control information__ _[(info)](#get-access-control-information)_.

## Get access control information
__Access control__ is based on information provided by the selected identity federation
and is presented in the form of a VO name and, optionally, a group name. Every user
with active membership in the specified VO/group will have full access to all resources
of the requested group project. Membership is managed with tools provided by the selected
identity federation, in a self-service manner.

If you already have VO/group, and already using services
provided by the __CESNET e-Infrastructure__ e.g. MetaCentrum, Data Care,
or you represent a project internal to __Masaryk University__,
please visit OpenID Connect User Profile according to your federation:
 - [EINFRA CESNET](https://login.cesnet.cz/oidc/manage/user/profile)
 - [EGI](https://aai.egi.eu/oidc/manage/user/profile)
 - [ELIXIR](https://login.elixir-czech.org/oidc/manage/user/profile)

and provide us with information that you see on the page. That is going to be __access control information__.

If you don't have VO/group or you know nothing about it, please contact MUNI Identity Management team
in order to create a new group within the Unified Login service.

 In the request, describe that you need a group for accessing MetaCentrum Cloud and provide the following information:
* Project/group name
* Project members

After creating a group, you will be provided with a graphical interface for managing group members within the Perun system.
And you will be able to make further adjustments to the member list on your own.
