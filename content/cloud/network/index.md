---
title: "Networking"
date: 2021-05-18T11:22:35+02:00
draft: false
---



For the networking in Cloud2 metacentrum we need to distinguish following scenarios
* personal project
* group project.


{{< hint danger >}}
**WARNING:**
     Please read the following rules:

   1. If you are using a [PERSONAL](/cloud/register/#personal-project)  project you have to use the `78-128-250-pers-proj-net` network to make your instance accesible from external network (e.g. Internet). Use `public-cesnet-78-128-250-PERSONAL` for FIP allocation, FIPs from this pool will be periodically released.
   2. If you are using a [GROUP](/cloud/register/#group-project)  project you may choose from the `public-cesnet-78-128-251-GROUP`, `public-muni-147-251-124-GROUP` or any other [GROUP](/cloud/register/#group-project)  network for FIP allocation to make your instance accesible from external network (e.g. Internet).
   3. Violation of the network usage may lead into resource removal and reducing of the quotas assigned.
{{< /hint >}}


## Networking in the personal vs. group projects

### Personal Project networking

Is currently limited to the common internal network. The network in which you should start your machine is called `78-128-250-pers-proj-net` and is selected by default when using dashboard to start a machine (if you do not have another network created). The floating IP adresses you need to access a virtual machine is `public-cesnet-78-128-250-PERSONAL`. Any other allocated floatin IP address and `external gateway` will be deleted. You cannot use router with the personal project and any previously created routers will be deleted.

### Group project

In group project situation is rather different. You cannot use the same approach as personal project (resources allocated in previously mentioned networks will be periodically released). For FIP you need to allocate from pools with `-GROUP` suffix (namely `public-cesnet-78-128-251-GROUP`, `public-muni-147-251-21-GROUP` or `public-muni-147-251-124-GROUP`).

{{< hint info >}}
**NOTICE**

If you use MUNI account, you can use private-muni-10-16-116 and log into the network via MUNI VPN or you can set up Proxy networking, which is described
[here](/cloud/network/#proxy-networking)
{{< /hint  >}}

#### Virtual Networks

MetaCentrum Cloud offers software-defined networking as one of its services. Users have the ability to create their own
networks and subnets, connect them with routers, and set up tiered network topologies.

Prerequisites:
* Basic understanding of routing
* Basic understanding of TCP/IP

For details, refer to [the official documentation](https://docs.openstack.org/horizon/train/user/create-networks.html).


#### Network creation

For group project you need to create internal network first, you may use autoallocated pool for subnet autocreation.
Navigate yourself towards **Network &gt; Networks** in the left menu and click on the **Create Network** on the right side of the window. This will start an interactive dialog for network creation.
![](images/1.png)
![](images/2.png)
Inside the interactive dialog:
1. Type in the network name
![](images/3.png)
2. Move to the **Subnet** section either by clicking next or by clicking on the **Subnet** tab. You may choose to enter network range manually (recommended for advanced users in order to not interfere with the public IP address ranges), or select **Allocate Network Address from a pool**. In the **Address pool** section select a `private-192-168`. Select Network mask which suits your needs (`27` as default can hold up to 29 machines, use IP calculator if you are not sure).
![](images/4.png)
3. For the last tab **Subnet Details** just check that a DNS is present and DHCP box is checked, alternatively you can create the allocation pool or specify static routes in here (for advanced users).
![](images/5.png)


{{< hint danger >}}
**NOTICE**

If you want to use CLI to create network, please go [here](/cloud/cli/#create-network)
{{< /hint >}}


#### Proxy networking
In your OpenStack instances you can you private or public networks. If you use private network and you need to access to the internet for updates etc.,
you can visit following [link](/cloud/faq/#issues-with-proxy-in-private-networks), where it is explained, how to set up Proxy connection.


#### Setup Router gateway (Required for Group projects)
Completing [Create Virtual Machine Instance](/cloud/quick-start/#create-virtual-machine-instance) created instance connected
to software defined network represented by internal network, subnet and router. Router has by default gateway address
from External Network chosen by cloud administrators. You can change it to any External Network with [GROUP](/cloud/register/#group-project) suffix, that
is visible to you (e.g. **public-muni-147-251-124-GROUP** or **public-cesnet-78-128-251-GROUP**). Usage of External Networks
with suffix PERSONAL (e.g. **public-cesnet-78-128-250-PERSONAL**) is discouraged. IP addresses from
PERSONAL segments will be automatically released from Group projects.
For changing gateway IP address follow these steps:

1. In **Network &gt; Routers**, click the **Set Gateway** button next to router.
If router exists with another settings, then use button Clear Gateway, confirm Clear Gateway.
If router isn't set then use button Create router and choose network.

2. From list of External Network choose **public-cesnet-78-128-251-GROUP**, **public-muni-147-251-124-GROUP** or any other [GROUP](/cloud/register/#group-project)  network you see.

 ![](images/network_routers-group.png)

Router is setup with persistent gateway.

#### Router creation

Navigate yourself towards **Network &gt; Routers** in the left menu and click on the **Create Router** on the right side of the window.
In the interactive dialog:
1. Enter router name and select external gateway with the `-GROUP` suffix.
![](images/r1.png)

Now you need to attach your internal network to the router.
1. Click on the router you just created.
2. Move to the **Interfaces** tab and click on the **Add interface**.
![](images/r2.png)
3. Select a previously created subnet and submit.
![](images/r3.png)


{{< hint info >}}
**NOTICE**

If you want to use CLI to manage routers, please go [here](/cloud/cli/#router-management)
{{< /hint >}}


{{< hint info >}}
**NOTICE**

Routers can also be used to route traffic between internal networks. This is an advanced topic not covered in this guide.
{{< /hint  >}}


#### Associate Floating IP


{{< hint danger >}}
**WARNING**

There is a limited number of Floating IP adresses. So please before you ask for more Floating IP address, visit and read [FAQ](/cloud/faq/#how-many-floating-ips-does-my-group-project-need)
{{< /hint  >}}



To make an instance accessible from external networks (e.g., The Internet), a so-called Floating IP Address has to be
associated with it.

1. In **Project &gt; Network &gt; Floating IPs**, select **Allocate IP to Project**. Pick an IP pool from which to allocate
   the address. Click on **Allocate IP**.

{{< hint info >}}
**NOTICE**

In case of group projects when picking an IP pool from which to allocate a floating IP address, please, keep in mind that you have to allocate
an address in the pool connected to your virtual router.
{{< /hint  >}}

{{< hint danger >}}
**WARNING**
Group projects can persistently allocate IPs only from External Network with GROUP suffix (e.g. public-muni-147-251-124-GROUP or public-cesnet-78-128-251-GROUP).
IPs from External Networks with suffix PERSONAL (e.g. public-cesnet-78-128-250-PERSONAL) will be released automatically.
{{< /hint  >}}

{{< hint info>}}
**NOTICE**
Please, keep an eye on the number of allocated IPs in <strong>Project &gt; Network &gt; Floating IPs</strong>. IPs
remain allocated to you until you explicitly release them in this tab. Detaching an IP from an instance is not sufficient
and the IP in question will remain allocated to you and consume your Floating IP quota.
{{< /hint >}}

1. In **Project &gt; Compute &gt; Instances**, select **Associate Floating IP** from the **Actions** drop-down menu for the
   given instance.

2. Select IP address and click on **Associate**.

  ![](images/network_floating_ip.png)

{{< hint info >}}
**NOTICE**

If you want to use CLI to manage FIP, please go [here](/cloud/cli/#floating-ip-address-management).
{{< /hint >}}


## Change external network in GUI

Following chapter covers the problem of changing the external network via GUI or CLI.

### Existing Floating IP release

First you need to release existing Floating IPs from your instances - go to **Project &gt; Compute &gt;  Instances**. Click on the menu **Actions** on the instance you whish to change and **Disassociate Floating IP** and specify that you wish to **Release Floating IP** WARN: After this action your project will no longer be able to use the floating IP address you released. Confirm that you wish to disassociate the floating IP by clicking on the **Disassociate** button. When you are done with all instances connected to your router you may continue with the next step.
![](images/instance1.png)

### Clear Gateway

Now, you should navigate yourself to the **Project &gt; Network &gt; Routers**. Click on the action **Clear Gateway** of your router. This action will disassociate the external network from your router, so your machines will not longer be able to access Internet. If you get an error go back to step 1 and **Disassociate your Floating IPs**.
![](images/clear-router1.png)

### Set Gateway

1. Now, you can set your gateway by clicking **Set Gateway**.
![](images/set-router1.png)

2. Choose network you desire to use (e.g. **public-cesnet-78-128-251**) and confirm.
![](images/set-router2.png)

### Allocate new Floating IP(s)

{{< hint danger >}}
**WARNING**
New floating IP address for router must be from same network pool which was selected as new gateway.
{{< /hint  >}}


1. Go to **Project &gt; Network &gt; Floating IPs** and click on the **Allocate IP to Project** button. Select **Pool** with the same value as the network you chose in the previous step and confirm it by clicking **Allocate IP**
![](images/allocate-fip.png)

2. Now click on the **Associate** button next to the Floating IP you just created. Select **Port to be associated** with desired instance. Confirm with the **Associate** button. Repeat this section for all your machines requiring a Floating IP.
![](images/associate-fip.png)
