---
title: "About metacentrum.cz cloud"
date: 2021-05-18T11:22:35+02:00
draft: false
---

## Hardware
MetaCentrum Cloud consist of 13 computational clusters containing 283 hypervisors
with sum of 9560 cores, 62 GPU cards and 184 TB RAM. For applications with special demands cluster
with local SSDs and GPU cards is available. OpenStack instances, object store and image store
can leverage more than 1.5 PTB highly available capacity provided by CEPH storage system.

## Software

MetaCentrum Cloud is built on top of OpenStack, which is a free open standard cloud computing platform
and one of the top 3 most active open source projects in the world. New OpenStack major version is
released twice a year. OpenStack functionality is separated into more than 50 services.

## Number of usage
More than 400 users are using MetaCentrum Cloud platform and more than 130k VMs were started last year.

## MetaCentrum Cloud current release

OpenStack Train

## Deployed services

Following table contains list of OpenStack services deployed in MetaCentrum Cloud. Services are separated
into two groups based on their stability and level of support we are able to provide. All services in production
group are well tested by our team and are covered by support of cloud@metacentrum.cz. To be able to support
variety of experimental cases we are planning to deploy several services as experimental, which can be useful
for testing purposes, but it's functionality won't be covered by support of cloud@metacentrum.cz.

| Service   | Description            | Type         |
|-----------|------------------------|--------------|
| cinder    | Block Storage service  | production   |
| glance    | Image service          | production   |
| heat      | Orchestration service  | production   |
| horizon   | Dashboard              | production   |
| keystone  | Identity service       | production   |
| monasca   | Monitoring service     | experimental |
| neutron   | Networking service     | production   |
| nova      | Compute service        | production   |
| Octavia   | Load Balancing Service | experimental |
| placement | Placement service      | production   |
| swift/s3  | Object Storage service | production   |
