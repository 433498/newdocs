---
title: "Frequently Asked Questions"
date: 2021-05-18T11:22:35+02:00
draft: false
---


## Where I can find how to use MetaCentrum Cloud effectively?
Read our [cloud best-practice tips](/cloud/register).

## What to expect from the cloud and cloud computing

[Migration of Legacy Systems to Cloud Computing](https://www.researchgate.net/publication/280154501_Migration_of_Legacy_Systems_to_Cloud_Computing) article gives the overwiew what to expect when joining a cloud with personal legacy application.

###  What are the cloud computing benefits?

The most visible [cloud computing](https://en.wikipedia.org/wiki/Cloud_computing) benefits are:
 * cost savings
 * online access to the cloud resources for everyone authorized
 * cloud project scalability (elasticity)
 * online cloud resource management and improved sustainability
 * security and privacy improvements
 * encouraged cloud project agility

## How do I register?
Follow instructions for registering in [MetaCentrum Cloud](/cloud/register).

## How do I migrate from legacy platforms?
Follow instructions for [migrating from CESNET-MetaCloud or OStack ICS MUNI](/cloud/migrate).

## Where do I report a problem?
First, try searching the documentation for an answer to your problem. If that does not yield results, open a
ticket with [cloud@metacentrum.cz](mailto:cloud@metacentrum.cz). When contacting user support, always
include your *username* (upper right corner of the web interface) and *domain* with
active *project* (upper left corner of the web interface) as well as a description of
your problem and/or an error message if available.

## What networks I can use to access my instances?
Personal projects can allocate floating IPs from *public-muni-147-251-124* and *private-muni-10-16-116*.
Group projects can currently allocate floating IPs from *public-cesnet-78-128-251* and *private-muni-10-16-116*.
IP addresses from *public-muni-147-251-124* allocated by users to group projects are released daily, so we encourage
using only *public-cesnet-78-128-251* and *private-muni-10-16-116* for group projects.
Follow instructions at [changing the external network](/cloud/network) in order to change your public network.

## Issues with network stability in Docker
OpenStack instances use 1442 bytes MTU (maximum transmission unit) instead of standard 1500 bytes MTU. Instance itself is
able to setup correct MTU with its counterpart via Path MTU Discovery. Docker needs MTU setup explicitly. Refer documentation for setting up
1442 MTU in [Docker](https://docs.docker.com/v17.09/engine/userguide/networking/default_network/custom-docker0/) or
[Kubernetes](https://docs.projectcalico.org/v3.5/usage/configuration/mtu).

## Issues with proxy in private networks
OpenStack instances can either use public or private networks. If you are using a private network and you need to access to the internet for updates etc.,
you can use muni proxy server *proxy.ics.muni.cz*. This server only supports HTTP protocol, not HTTPS. To configure it you must also consider what applications
will be using it, because they can have their own configuration files, where this information must be set. If so, you must find particular setting and set up there
mentioned proxy server with port 3128. Mostly applications use following setting, which can be done by editing file `/etc/environment` where you need to add a line
`http_proxy="http://proxy.ics.muni.cz:3128/"`. And then you must either restart your machine or use command `source /etc/environment`.

## How many floating IPs does my group project need?
One floating IP per project should generally suffice. All OpenStack instances are deployed on top of internal OpenStack networks. These internal networks are not by default accessible from outside of OpenStack, but instances on top of same internal network can communicate with each other.

To access internet from an instance, or access instance from the internet, you could allocate floating public IP per instance. Since there are not many public IP addresses available and assigning public IP to every instance is not security best practise, both in public and private clouds these two concepts are used:
* **internet access is provided by virtual router** - all new OpenStack projects are created with *group-project-network* internal network connected to virtual router with public IP as a gateway. Every instance created with *group-project-network* can access internet through NAT provided by it's router by default.
* **accessing the instances:**
 * **I need to access instances by myself** - best practice for accessing your instances is creating one server with floating IP called [jump host](https://en.wikipedia.org/wiki/Jump_server) and then access all other instances through this host. Simple setup:
   1. Create instance with any Linux.
   2. Associate floating IP with this instance.
   3. Install [sshuttle](https://github.com/sshuttle/sshuttle) on your client.
   4. `sshuttle -r root@jump_host_fip 192.168.0.1/24`. All your traffic to internal OpenStack network *192.168.0.1/24* is now tunneled through jump host.
 * **I need to serve content (e.g. webservice) to other users** - public and private clouds provide LBaaS (Load-Balancer-as-a-Service) service, which proxies users traffic to instances. MetaCentrum Cloud provides this service in experimental mode - [documentation](/cloud/gui#lbaas)

In case, that these options are not suitable for you usecase, you can still request multiple floating IPs.

## I can't log into openstack, how is that possible ?
The most common reason why you can't log into your openstack account is because your membership in Metacentrum has expired. To extend your membership in Metacentrum,
you can visit [https://metavo.metacentrum.cz/en/myaccount/prodlouzeni](https://metavo.metacentrum.cz/en/myaccount/prodlouzeni).