---
title: "Quick Start"
date: 2021-05-18T11:22:35+02:00
draft: false
---


The following guide will take you through the steps necessary to start your first virtual machine instance.

Prerequisites:
* Up-to-date web browser
* Active account in MetaCentrum (https://metavo.metacentrum.cz/en/application/index.html)
* Basic knowledge of SSH (for remote connections)



## Sign In

The dashboard is available at [https://dashboard.cloud.muni.cz](https://dashboard.cloud.muni.cz).

1. Select `EINFRA CESNET`.

{{< hint info >}}
**NOTICE**

Users of the Czech national e-infrastructure (also MetaCentrum/CERIT-SC/MUNI users) should always select <strong>EINFRA CESNET</strong>.
{{< /hint >}}

{{< hint info >}}
**NOTICE**
International users may choose <strong>EGI Check-in</strong>, <strong>DEEP AAI</strong> or <strong>ELIXIR AAI</strong>, depending on their membership in these projects.
{{< /hint >}}


{{< hint danger >}}
**WARNING**

If you use multiple accounts, you should use the one that you need for your corresponing work.
{{< /hint >}}

2. Click on **Sign In**.

  ![](images/sign_in1.png)

3. Select your institution from the drop-down list.

{{< hint info >}}
**NOTICE**

You can use the search box at the top as a filter.
{{< /hint >}}

4. Provide your institution-specific sign-in credentials.
5. Wait to be redirected back to our dashboard.

{{< hint info >}}
**NOTICE**
If your attempts repeatedly result in an error message about projects, make sure you have an active MetaCentrum account
and that you are selecting the correct institution when attempting to log in. If the problem persists, please, contact user
support.
{{< /hint >}}

## Create Key Pair

All virtual machine instances running in the cloud have to be accessed remotely. The most common way of accessing
an instance remotely is SSH. Using SSH requires a pair of keys - a public key and a private key.

1. In **Project &gt; Compute &gt; Key Pairs**, click the **Create Key Pair** or **Import Public Key ** button.
      * **Import Key Pair** if existing SSH key on your local computer, but not listed as available, then import public key using button **Import Public Key**
        * Insert Key Pair Name
        * Select SSH key for Key Type
        * Load Public Key from a file or copy/paste public key content in the field
      * **Create key Pair** if any public key not available
        * Use button **Create Key Pair**
        * Insert Key Pair Name. Avoid using special characters, if possible.
        * Select SSH key for Key Type
        * Use button **Create KeyPair**
        * Copy Private Key to Clipboard and save it to the ~/.ssh/id_rsa on your local computer
        * Confirm using button **Done**
        * Now the public key is available down on the page. Use arrow before key name to show public part. Copy this public key to the file ~/.ssh/id_rsa.pub on your local computer

      ![](images/key-pair-create.png)

2. Set Access Privileges on .ssh Folder using commands
```
chmod 700 .ssh/
chmod 644 .ssh/id_rsa.pub
chmod 600 .ssh/id_rsa
```

For details, refer to [the official documentation](https://docs.openstack.org/horizon/train/user/configure-access-and-security-for-instances.html).

## Update Security Group

In MetaCentrum Cloud, all incoming traffic from external networks to virtual machine instances is blocked by default.
You need to explicitly allow access to virtual machine instances and services via a security group.

You need to add two new rules to be able to connect to your new instance (or any instance using the given security group).
This is similar to setting up firewall rules on your router or server. If set up correctly, you will be able to access
your virtual machine via SSH from your local terminal.

1. Go to **Project &gt;  Network &gt; Security Groups**. Click on **Manage Rules**, for the **default** security group.

2. Click on **Add rule**, choose **SSH** and leave the remaining fields unchanged.
   This will allow you to access your instance.

3. Click on **Add rule**, choose **ALL ICMP** and leave the remaining fields unchanged.
   This will allow you to `ping` your instance.

  ![](images/network_secutity_groups_rules.png)


{{< hint danger >}}
**WARNING**
You have 2 possibilities how to configure security groups policy. One is through CIDR which specifies rules for concrete network range. The second one specifies
rules for members of specified security group, i.e. policy will be applied on instances who belong to selected security group.
{{</hint>}}


For details, refer to [the official documentation](https://docs.openstack.org/horizon/train/user/configure-access-and-security-for-instances.html).

## Create Virtual Machine Instance

1. In **Compute &gt; Instances**, click the **Launch Instance** button.

  ![](images/instance_launch.png)

2. Choose name, description, and the number of instances.
   If you are creating more instances, `-%i` will be automatically appended to the name of each instance.

  ![](images/instance_launch_details.png)

3. Choose an image from which to boot the instance. Image will be automatically copied to a persistent volume
   that will remain available even after the instance has been deleted.

  ![](images/instance_launch_source.png)

4. Choose the size of your instance by selecting a flavor. Additional volumes for data can be attached later on.

  ![](images/instance_launch_flavor.png)

  On following image you can also find details about concrete flavor in highlighted sections

  ![](images/flavor_detail.png)

5. Select Network

      * For personal project select personal-project-network-subnet from network 78-128-250-pers-proj-net
        ![](images/instance_launch_network-user.png)
      * For group project select group-project-network-subnet from network group-project-network (check if [Router gateway](/network#associate-floating-ip") is set)
        ![](images/instance_launch_network-group.png)
6. Key pair
      * **If public key was imported already**, add existing key to the instance and continue using button **Next**
      * **Import Key Pair** if you have existing SSH key on your local computer, but not listed as available, then import public key using button **Import Key**
        * Continue steps in section [Create Key Pair](#create-key-pair) -> Import Key Pair
        * Add key and continue using button **Next**
      * **Create key Pair** if you don't have any public key available
        * Use button **Create Key Pair**
        * Continue steps in section [Create Key Pair](#create-key-pair) -> Create Key Pair
        * Add key and continue using button **Next**
    ![Select Key pair](/cloud/quick-start/images/instance_launch_key_pair.png)

7. __(optional)__ On the Configuration tab, you can use `user_data` to customize your instance at boot.
    ![](/cloud/quick-start/images/instance_launch_configuration.png)
   See [cloud-init](https://cloud-init.io/) for details.

8. Use button Launch Instance to initialize new instance

9. Wait until instance initialization finishes and [Associate Floating IP](/cloud/network/#associate-floating-ip). For group project always select the same network as used in [Router gateway](/network/#setup-router-gateway-required-for-group-projects)
    ![](images/instance_associate_ip.png)

10. Login using your SSH key as selected in Key pair above
Connect to the instance using **ssh image login**, [id_rsa key registered in Openstack](#create-key-pair) and [Floating IP](/network/#associate-floating-ip).


{{< hint info >}}
**NOTICE**
On Linux and Mac you can use already present SSH client. On Windows there are other possibilities how to connect via SSH. One of the most common is [PuTTy](https://en.wikipedia.org/wiki/PuTTY) SSH client. How to configure and use PuTTy you can visit our tutorial [here](/cloud/putty/#putty).
{{</hint>}}


To get **ssh image login** of corresponding image  go to **Project &gt;  Compute &gt; Images**. There you will have list of all available images and then you have to click on name of one you want.
  ![](images/ssh_login1.png)

And then you must check section **Custom Properties** where you will see a property **default_user** which is **ssh image login**. In picture **ssh image login** for that image is "debian".
  ![](images/ssh_login2.png)


Then you can connect to your instance using following command:

```
ssh -A -X -i ~/.ssh/id_rsa <ssh_image_login>@<Floating IP>
```

So it can look like:

```
ssh -A -X -i ~/.ssh/id_rsa debian@192.168.18.15
```
```
-A      Enables forwarding of the authentication agent connection.
-X      Enables X11 forwarding.
```
| OS | login for ssh command|
|---|---|
|Debian |debian|
|Ubuntu| ubuntu|
|Centos| centos|



For details, refer to [the official documentation](https://docs.openstack.org/horizon/train/user/launch-instances.html).


## Setup Router gateway (Required only for GROUP projects!!)

{{< hint danger >}}
**WARNING**
If you have [GROUP](/cloud/register/#group-project) project, you need to configure
Router gateway for your network. [Here](/cloud/network/#setup-router-gateway-required-for-group-projects) you can find the manual.
{{</hint>}}


## Associate Floating IP


{{< hint danger >}}
**WARNING**
To make an instance accessible from external networks (e.g., The Internet), a so-called Floating IP Address has to be
associated with it. [Here](/cloud/network/#associate-floating-ip) you can find the manual.
{{</hint>}}

## Create Volume

When storing a large amount of data in a virtual machine instance, it is advisable to use a separate volume and not the
root file system containing the operating system. It adds flexibility and often prevents data loss. Volumes can be
attached and detached from instances at any time, their creation and deletion are managed separately from instances.

1. In **Project &gt; Volumes &gt; Volumes**, select **Create Volume**.
2. Provide name, description and size in GBs. If not instructed otherwise, leave all other fields unchanged.
3. Click on **Create Volume**.
4. __(optional)__ In **Project &gt; Compute &gt; Instances**, select **Attach Volume** from the **Actions** drop-down menu for the
   given instance.
5. __(optional)__ Select **Volume ID** from the drop-down list and click **Attach Volume**.

For details, refer to [the official documentation](https://docs.openstack.org/horizon/train/user/manage-volumes.html).

## Instance resize
If you find out that you need to select another flavor for your instance, here is a tutorial how to do it:

Before resizing u should save all your unsaved work and you should also consider making data backup in case if something goes wrong.

First you must go to your instance and then you select **Resize Instance**

  ![](images/resize.png)

After that you select what flavor you want instead of your current one and then you confirm your choice

  ![](images/select.png)

Then you just have to either confirm or revert selected changes in instance menu

  ![](images/resize_confirm.png)
