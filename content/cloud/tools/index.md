---
title: "Cloud Tools"
date: 2021-05-18T11:22:35+02:00
draft: false
---

On this address [https://gitlab.ics.muni.cz/cloud/cloud-tools](https://gitlab.ics.muni.cz/cloud/cloud-tools) you can find a docker container with all modules required for cloud management, if you are interested in managing your cloud platform via CLI. If so, you can check our guide how to use CLI cloud interface [here](/cloud/cli/).