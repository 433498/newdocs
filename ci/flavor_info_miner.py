import yaml
import argparse
import csv


#Author: Jaromir Hradil 506487@mail.muni.cz

#This script gathers info from hiera about flavors
#Then it generates table in Markdown format.

#Setting arguments
parser = argparse.ArgumentParser(description='This script generates table with flavor info.')

parser.add_argument('--hiera-file', type=str, required=True, help='Path for hiera file flavors.yaml')

parser = parser.parse_args()

#Opening file
with open(parser.hiera_file,'r') as f:
    data = yaml.safe_load(f)


data = data['cloud::profile::kolla::nova::controller::os_flavors']



#Names for columns
details = ['Flavor name' , 'CPU' , 'RAM (in GB)' , 'HPC' , 'SSD' , 'Disc throughput (in MB per second)' , 'IOPS', 'Net average througput (in MB per second)', 'GPU']

blacklist = ['cerit.hde-half', 'cerit.hde', 'meta.mema2', 'csirtmu.tiny1x2', 'csirtmu.tiny1x4', 'csirtmu.small2x4', 'csirtmu.small2x8', 'csirtmu.medium4x8', 'csirtmu.medium4x16',
'csirtmu.large8x16', 'csirtmu.large4x32', 'csirtmu.large8x32', 'csirtmu.jumbo16x32', 'csirtmu.jumbo8x64', 'csirtmu.jumbo16x64', 'du.perftest', 'admin.scaletest', 'hpc.18core-64ram-dukan',
'hpc.8core-32ram-dukan']

blacklist_dict = dict.fromkeys(blacklist)



final_out = []


#Printing specified data
for x in range(0,len(data)):


    if(data[x]['name'] in blacklist_dict):
        continue

    if not ('aggregate_instance_extra_specs:hpc' in data[x]['extra_specs']):
        hpc = 'No'
    else:
        hpc = 'No' if (data[x]['extra_specs']['aggregate_instance_extra_specs:hpc'] == 'false') else 'Yes'

    if not ('aggregate_instance_extra_specs:ssd' in data[x]['extra_specs']):
        ssd = 'No'
    else:
        ssd = 'No' if (data[x]['extra_specs']['aggregate_instance_extra_specs:ssd'] == 'false') else 'Yes'

    gpu = 'No'

    if ('pci_passthrough:alias' in data[x]['extra_specs']):

        if(data[x]['extra_specs']['pci_passthrough:alias'].find('gpu') != -1):
            gpu = 'Yes'

    name = data[x]['name']
    vcpus = data[x]['vcpus']
    ram = data[x]['ram']/1024

    #more readable format
    disk = data[x]['extra_specs']['quota:disk_total_bytes_sec'] / 1000000 if 'quota:disk_total_bytes_sec' in data[x]['extra_specs'] else 'Unlimited'
    net =  data[x]['extra_specs']['quota:vif_outbound_average'] / 1000  if 'quota:vif_outbound_average' in data[x]['extra_specs'] else 'Unlimited'
    iops =  data[x]['extra_specs']['quota:disk_total_iops_sec'] if 'quota:disk_total_iops_sec' in data[x]['extra_specs'] else 'Unlimited'

    #Printing final table sorted
    final_out.append([f'{name}', f'{vcpus}',  f'{ram:.0f}', f'{hpc}' , f'{ssd}',\
          f'{disk}', f'{iops}', f'{net}',  f'{gpu}'])




with open('flavors.csv', 'w', newline='') as csvfile:
    
    writer = csv.DictWriter(csvfile, fieldnames=details)
    writer.writeheader()
    
    for x in range(0,len(final_out)):
#['Flavor name' , 'CPU' , 'RAM (in GB)' , 'HPC' , 'SSD' , 'Disc throughput (in MB per second)' , 'IOPS', 'Net average througput (in MB per second)', 'GPU']
        writer.writerow({details[0] : final_out[x][0], details[1] : final_out[x][1], details[2] : final_out[x][2], details[3] : final_out[x][3],details[4] : final_out[x][4],\
            details[5] : final_out[x][5],details[6] : final_out[x][6],details[7] : final_out[x][7],details[8] : final_out[x][8]})